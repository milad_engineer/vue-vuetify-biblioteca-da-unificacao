import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import FirebaseVue from './firebase'

Vue.config.productionTip = false
Vue.component('Footer')
Vue.use(FirebaseVue)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
