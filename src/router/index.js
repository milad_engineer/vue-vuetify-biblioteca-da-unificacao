import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter ({mode: 'history', routes})

// router.beforeEach((to, from, next) => {
//     if(!window.uid && to.path != 'userpage'){
//         next({path: '/'});
//     } else {
//         next();
//     }
// })

export default router
