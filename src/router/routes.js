export default [
    //@@@@@----- PRINCIPAIS
    {
    path: '/',
    name: 'Home',
    // route level code-splitting
    // isso gera um arquivo separado para esta rota
    // que e carregado no momento que eh chamado
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },

    //@@@@@----- DE LOGIN

    //@@@@@----- DO USUARIO LOGADO

    //@@@@@----- PALAVRA

    {
    path: '/curtas',
    name: 'Frases Curtas',
    component: () => import(/* webpackChunkName: "curtas" */ '../views/palavra/curtas.vue')
    },

    {
    path: '/guias',
    name: 'Guias',
    component: () => import(/* webpackChunkName: "chegar" */ '../views/palavra/guias.vue')
    },

    //@@@@@----- IMAGENS

    {
    path: '/vps',
    name: 'Fotos dos VPs',
    component: () => import(/* webpackChunkName: "vps" */ '../views/imagens/vps.vue')
    },

    //@@@@@----- VIDEOS

    {
    path: '/presidente',
    name: 'Guias do Presidente Koichi',
    component: () => import(/* webpackChunkName: "presidente" */ '../views/videos/presidente.vue')
    },

    {
    path: '/pd',
    name: 'Conferências do PD',
    component: () => import(/* webpackChunkName: "pd" */ '../views/videos/pd.vue')
    },

    {
    path: '/causa',
    name: 'Conferências da Causa',
    component: () => import(/* webpackChunkName: "causa" */ '../views/videos/causa.vue')
    },

    //@@@@@----- HOME GROUP

    {
    path: '/guiashg',
    name: 'Guias de HG',
    component: () => import(/* webpackChunkName: "guiashg" */ '../views/hg/guiashg.vue')
    },

    {
    path: '/quebragelo',
    name: 'Quebra Gelo',
    component: () => import(/* webpackChunkName: "balnearios" */ '../views/hg/quebragelo.vue')
    },

    

    

    

    

    

    
]
