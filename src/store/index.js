import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    counter: 10,
    token: '',
  },
  mutations: {
    increment (state){
      state.counter++;
    },
  },
  actions: {
    actionIncrement({commit}){
      commit('increment');
    }
  },
  modules: {
  },
  getters: {
    counter: state => state.counter
  }
})
